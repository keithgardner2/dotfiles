alias gs="git status"
alias gl="git log"
alias gcp="git cherry-pick"

alias gss="git stash show -p"
alias gsss="git stash show"
alias gsp="git stash pop"
alias gsd="git stash drop"

alias grb="git for-each-ref --sort=-committerdate refs/heads/"

alias gco="git checkout"
alias gd="git diff"

alias c="clear"

alias wds="./bin/webpack-dev-server"

PATH=$PATH:/usr/local/Cellar/postgresql/10.5/bin/psql

if [[ -f "$(brew --prefix)/opt/mcfly/mcfly.bash" ]]; then
  source "$(brew --prefix)/opt/mcfly/mcfly.bash"
fi

export PATH="$HOME/.rbenv/bin:$PATH"
export PATH="$HOME/.rbenv/shims:$PATH"
PATH=$PATH:/usr/local/Cellar/postgresql/12.1/bin/psql
eval "$(rbenv init -)"

